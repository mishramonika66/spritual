@extends('admin.layouts.layout')
<!--@section('title', 'Zen Body Massage')-->
<!--/ in case you want to write JS, write here/-->
@section('custom_js')
<!--<script src="{{url('/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>-->
@parent
<!--/ in case you want to write JS, write here/-->



<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
@parent
<!--/ in case you want to write CSS, write here/-->

<!--/ in case you want to write CSS, write here/-->
@endsection

@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')

<!-- BEGIN: Content-->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
        <small>All SubCategories</small>
      </h1>
      <ol class="breadcrumb">
         <a href="{{url('admin/addsubcategory')}}" class="btn btn-block btn-default btn-flat">Add SubCategory</a>

<!--          <li ><button type="button" class="btn btn-block btn-default btn-flat">Add Category</button></li>-->
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active"></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All SubCategories</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 
                  <th>Name</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Action</th>
                 
                </tr>
                </thead>
                <tbody>
            
                  <?php if (isset($subcategories) && $subcategories->count()) { ?>
                       <?php foreach ($subcategories as $value) { ?>
                                        <tr>
                                        <td><?php echo $value->name ?></td>
                                        <td><img style="width:50px;" src="{{asset($value->image)}}"></img></td>
                                        <td><?php if( $value->status == 1){ echo "Active" ;} else {"Inactive";}?></td>
                                        <td>
                                            <a href =""><i class="fa fa-fw fa-remove"></i></a>
                                            <a href =""><i class="fa fa-fw fa-edit"></i></a>                                              
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                   </tbody>
                <tfoot>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

@endsection