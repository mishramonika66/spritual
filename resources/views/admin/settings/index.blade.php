@extends('admin.layouts.layout')
@section('title', 'Admin About Us')
<!--/ in case you want to write JS, write here/-->

@section('custom_js')

@parent
<!--/ in case you want to write JS, write here/-->
<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{url('public/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/app-assets/css/plugins/extensions/swiper.css')}}">

<!-- END: Page CSS-->

@parent
<!--/ in case you want to write CSS, write here/-->

<!--/ in case you want to write CSS, write here/-->
@endsection

@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    
</div>
@endsection