@extends('admin.layouts.layout')
@section('title', 'Spiritual')
<!--/ in case you want to write JS, write here/-->
@section('custom_js')
 <script src="{{url('/app-assets/js/scripts/pages/page-users.js')}}"></script>
@parent
<!--/ in case you want to write JS, write here/-->
<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
<link rel="stylesheet" type="text/css" href="{{url('/app-assets/css/pages/page-users.css')}}">
@parent
<!--/ in case you want to write CSS, write here/-->




<!--/ in case you want to write CSS, write here/-->
@endsection
@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')

    
    <!-- END: Page CSS-->


    <!-- BEGIN: Content-->
    
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add SubSubCategory

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">SubSubCategory</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add SubSubCategory</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{ url('admin/subsubcategorystore')}}" method="POST" enctype="multipart/form-data" class="">
                        {{ csrf_field() }}

                        @if ($errors->any())
                        
               
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                           
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                         
                            {{ session()->get('message') }}
                        </div>
                        
                        @endif
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Image</label>
                                <input type="file"  name ="image" id ="image" class="form-control" id="exampleInputFile">

                            </div>
                            
                             <div class="form-group">
                                <label>Select SubCategory</label>
                                <select class="form-control" name ="subcategory_id" id ="subcategory_id">
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Select</label>
                                <select class="form-control" name ="status" id ="status">
                                    <option value="1">Active</option>
                                    <option value="0">In Active</option>
                                </select>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

                <!-- Form Element sizes -->

                <!-- /.box -->


                <!-- /.box -->

                <!-- Input addon -->

                <!-- /.box -->

            </div>
            <!--/.col (left) -->
            <!-- right column -->

            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

   

@endsection
