@extends('front.layout.app')

@section('title', 'Home | Fanshub')
<!--/ in case you want to write JS, write here/-->
@section('custom_js')
@parent
<!--/ in case you want to write JS, write here/-->


<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
@parent
<!--/ in case you want to write CSS, write here/-->




<!--/ in case you want to write CSS, write here/-->
@endsection
@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')



<section class="about-us new-block pdt-100">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="img-block">
							<div class="img-holder">
								<img src="{{ url('/public/assets') }}/images/pimg.png" alt="" class="img-responsive">
							</div>
						</div>	
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7">
						<div class="txt-block">
							<h2>Welcome to fanshub</h2>
							<p>Where Sugar Babies enjoy a life of luxury by being pampered with fine dinners, exotic trips and allowances. In turn, Sugar Daddies or Mommas find beautiful members to accompany them at all times.</p>
							<p>We want relationships to be balanced. We give our members a place for this to happen. Enjoy a life of luxury by being pampered with fine dinners, exotic trips and allowances. In turn, Sugar Daddies or Mommas find beautiful members.
							exotic trips and allowances. In turn, Sugar Daddies or Mommas find beautiful members.</p>
						</div>
					</div> 
				</div>
			</div>
		</section><!-- about-us -->

		

		


		

		<section class="member-feedback new-block pdtb-100">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="title2">
							<h2 class="clr1 fz35">Fanshub Member Feedback</h2>
							<div class="clearfix"></div>
							<p class="fz20">Aliquam a neque tortor. Donec iaculis auctor turpis. Eporttitor<br> mattis ullamcorper urna. Cras quis elementum</p>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="testimonial owl-carousel owl-theme">
							<div class="item">
								<div class="new-block">
									<div class="block-stl3">
										<i class="flaticon-left-quote"></i>
										<p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
										<i class="flaticon-right-quotation-sign"></i>
										<div class="arrow-down"></div>
									</div>
									<div class="sm-block">
										<div class="img-holder">
											<img src="images/tm1.jpg" alt="">
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="new-block">
									<div class="block-stl3">
										<i class="flaticon-left-quote"></i>
										<p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
										<i class="flaticon-right-quotation-sign"></i>
										<div class="arrow-down"></div>
									</div>
									<div class="sm-block">
										<div class="img-holder">
											<img src="images/tm2.jpg" alt="">
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="new-block">
									<div class="block-stl3">
										<i class="flaticon-left-quote"></i>
										<p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
										<i class="flaticon-right-quotation-sign"></i>
										<div class="arrow-down"></div>
									</div>
									<div class="sm-block">
										<div class="img-holder">
											<img src="images/tm3.jpg" alt="">
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="new-block">
									<div class="block-stl3">
										<i class="flaticon-left-quote"></i>
										<p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
										<i class="flaticon-right-quotation-sign"></i>
										<div class="arrow-down"></div>
									</div>
									<div class="sm-block">
										<div class="img-holder">
											<img src="images/tm2.jpg" alt="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


				




@endsection