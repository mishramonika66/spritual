@extends('front.layout.app')

@section('title', 'Home | Fanshub')
<!--/ in case you want to write JS, write here/-->
@section('custom_js')
@parent
<!--/ in case you want to write JS, write here/-->


<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
@parent
<!--/ in case you want to write CSS, write here/-->




<!--/ in case you want to write CSS, write here/-->
@endsection
@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')

<style>
    .form.log{background-color:#c8c8c8; padding:30px;}
    .field-wrap{margin-bottom:10px;}
    .field-wrap label{width:100%; margin-bottom:5px;}
    .field-wrap input{width:100%; height:40px; padding:6px 12px;}
    .field-wrap button{background-color: #d826a8; padding: 15px; width: 100%; color: #fff; text-transform: uppercase; border: 0; font-weight: 600;;}
    .field-wrap button:hover{background-color: #bd1991;}
</style>


<script>

//    function  create() {
//
//        $('#form').submit(function (e) {
//           
//           
//            var formData = new FormData($(this)[0]);
//           
//            const FORM = $(this);
//            alert(formData);
//            console.log(formData);return false;
//            $.ajax({
//                url: "/create",
//                type: "POST",
//                data: formData,
//                success: function (msg) {
//                    $("#responseDiv").html(msg);
//                    FORM.trigger('reset');
//                    console.log(msg)
//                },
//                error: function (error) {
//                    $("#responseDiv").html(error.responseText);
//
//                },
//
//                cache: false,
//                contentType: false,
//                processData: false
//            });
//
//
//        });
//    }




    function create() {

        $('#form').submit(function (e) {

            var formData = new FormData($(this)[0]);

            const FORM = $(this);
            console.log(formData);
           
            $.ajax({
                url: "<?php echo url('create') ?>",
                type: "POST",
                data: formData,
                headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                               },

                beforeSend: function () {
                    $("#btnSubmit").attr("disabled", true);

                    setTimeout(function () {
                        FORM.trigger('reset'); // Reset form fields after success
                        $("#btnSubmit").attr("disabled", false);
                        $("#responseDiv").html('<p class="text-center coverage-txt" role="alert">Thank you for contacting us. You will get a reply within 24 hours. </p>');
                    }, 4000);

                },
                success: function (msg) {
                    msg = JSON.parse(msg);
                    $("#responseDiv").html(msg.message);
                    if (msg.success == 1) {
                        FORM.trigger('reset'); // Reset form fields after success
                    }
                },
                error: function (error) {
                    $("#responseDiv").html(error.responseText);
                },
                complete: function () {
                    $("#btnSubmit").attr("disabled", false);
                    //
                },
                cache: false,
                contentType: false,
                processData: false
            });
            e.preventDefault();

        });
    }
</script>


<section class="send-note new-block pdtb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="form log">

                    <h2 class="text-center">Sign Up</h2><br>

                    <div class="tab-content">


                        <div id="login">   
                            <form  method="POST" id="form" autocomplete="off" enctype="multipart/form-data">
                                 {{ csrf_field() }}


                                <div class="field-wrap">
                                    <label>
                                        Name*
                                    </label>
                                    <input type="text" name ="name" id="name" autocomplete="off"/>
                                </div>
                                <div class="field-wrap">
                                    <label>
                                        Email
                                    </label> 
                                    <input type="email" name ="email" id="email" autocomplete="off"/>
                                </div>
                                <div class="field-wrap">
                                    <label>
                                        Mobile Number
                                    </label>
                                    <input type="text"  name ="mobile" id ="mobile" autocomplete="off"/>
                                </div>

                                <!--                                <div class="field-wrap">
                                                                    <label>
                                                                        Password<span class="req">*</span>
                                                                    </label>
                                                                    <input type="password" id ="npassword" autocomplete="off"/>
                                                                </div>
                                                                <div class="field-wrap">
                                                                    <label>
                                                                        Confirm Password<span class="req">*</span>
                                                                    </label>
                                                                    <input type="password"  id = "cpassword" autocomplete="off"/>
                                                                </div>-->

                                <br>
                                <div class="field-wrap">
                                    <!--                                    <button class="button button-block"/>Sign Up</button>-->
                                    <input type="submit" onclick="create();" class="button button-block" value="Sign Up">

                                </div>
                                <h2 class="text-center">OR</h2>
                                <div class="loin-i">
                                    <ul>
                                        <li class="f"><a href="">Facebook</a></li>
                                        <li class="s"><a href="">Google</a></li>
                                    </ul>

                                </div>
                                <p style="margin-bottom: 0; margin-top: 20px;" class="forgot">Already have an account? <a href="#">Login</a></p>
                            </form>

                        </div>

                    </div><!-- tab-content -->

                </div></div></div> <!-- /form -->	
</section>








@endsection