@extends('front.layout.app')

@section('title', 'Home | Fanshub')
<!--/ in case you want to write JS, write here/-->
@section('custom_js')
@parent
<!--/ in case you want to write JS, write here/-->


<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
@parent
<!--/ in case you want to write CSS, write here/-->




<!--/ in case you want to write CSS, write here/-->
@endsection
@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')




	

		<section class="send-note new-block pdtb-100">
			<form>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="title2">
								<h2 class="fz35">Send Us a Note</h2>
								<div class="clearfix"></div>
								<p class="fz20">Aliquam a neque tortor. Donec iaculis auctor turpis. Eporttitor<br> mattis ullamcorper urna. Cras quis elementum</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Enter your first name">
							</div>	
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Enter your last name">
							</div>	
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Enter your email">
							</div>	
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Enter your contact number">
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<textarea class="form-control" placeholder="Enter your message.."></textarea>
							</div>	
						</div>
						<div class="col-lg-12">
							<div class="form-group text-center">
								<div class="c-btn btn1">Send</div>
							</div>	
						</div>
					</div>
				</div>
			</form>
		</section>







@endsection