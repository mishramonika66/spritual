<!doctype html>
<html lang="en">
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Monika Mishra">
        <meta name="generator" content="">
        <title>{{ env('APP_NAME') }} - @yield('title')</title>
        <meta name="description" content="{{ env('APP_NAME') }}"/>
        <meta name="keywords" content="{{ env('APP_NAME') }}"/>
        <!-- twitter card starts from here, if you don't need remove this section -->
        <!-- <meta name="twitter:card" content=""/> -->
        <meta name="twitter:site" content="{{ env('APP_NAME') }}"/>
        <meta name="twitter:url" content="{{ url('/') }}"/>
        <meta name="twitter:title" content="{{ env('APP_NAME') }}"/>
        <!-- maximum 140 char -->

        <meta name="twitter:description" content="{{ env('APP_NAME') }}"/>

        <!-- maximum 140 char -->

        <meta name="twitter:image" content=""/>

        <!-- when you post this page url in twitter , this image will be shown -->
        <!-- twitter card ends from here -->

        <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->

        <meta property="og:title" content="{{ env('APP_NAME') }}"/>
        <meta property="og:url" content="{{ url('/') }}"/>
        <meta property="og:locale" content="en_IN"/>
        <meta property="og:site_name" content="{{ env('APP_NAME') }}"/>

        <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->

        <meta property="og:type" content="website"/>
        <meta property="og:image" content=""/>

        <!-- when you post this page url in facebook , this image will be shown -->
        <!-- facebook open graph ends from here -->

        <link rel="canonical" href="{{ url('/') }}">

        <link href="{{ url('/public/assets') }}/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('/public/assets') }}/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ url('/public/assets') }}/css/style.css">
        <link rel="stylesheet" href="{{ url('/public/assets') }}/css/responsive.css">
        <link rel="stylesheet" href="{{ url('/public/assets') }}/css/colors.css">
        <style>


        </style>
    </head>
    <body>
        <!-- Loader -->

        <div id="wrapper">

            @include('front.layout.header')


            <!-- header -->
            <div class="banner new-block">
                <!--/ 
                
                
                View  
                
                
                
                /-->
                @yield('content')
                <!--/ 
                
                
                View  
                
                
                
                /-->
            </div>


            @include('front.layout.sidenav')
            
              
            <script>
                function openNav() {
                    document.getElementById("mySidenav").style.width = "250px";
                }

                /* Set the width of the side navigation to 0 */
                function closeNav() {
                    document.getElementById("mySidenav").style.width = "0";
                }
            </script>


            <!-- Include jQuery -->
            <script src="{{ url('/public/assets') }}/js/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="{{ url('/public/assets') }}/js/bootstrap.min.js"></script>
            <!-- Plugins -->
            <script src="{{ url('/public/assets') }}/js/plugins.js"></script>
            <!-- Googleapis -->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBP1lPhUhDU6MINpruPDinAzXffRlpzzFo"></script>
            <!-- Custom -->
            <script src="{{ url('/public/assets') }}/js/custom.js"></script>

    </body>

</html>
@include('front.layout.footer')