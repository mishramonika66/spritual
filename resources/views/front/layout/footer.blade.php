<footer class="r-footer2 new-block pdtb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="footer-block">
                    <div class="footer-head">
                        <img src="{{ url('/public/assets') }}/images/f-logo2.png" alt="" class="f-logo img-responsive">
                    </div>
                    <p class="f-about-txt">Aliquam a neque tDoneiaculis auctor turpis.Eportttristique mattis Vestibulum pretm.</p>

                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="footer-block pdleft25">
                    <div class="footer-head">
                        <h3>About Us :</h3>
                    </div>
                    <ul class="list-unstyled">
                        <li><a href="{{url('/about')}}">About Us</a></li>
                        <li><a href="{{url('/contact')}}">Contact Us</a></li>
                        <li><a href="{{url('/feed')}}">Feeds</a></li>
                        <li><a href="{{url('/faq')}}">Faq</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="footer-block pdleft25">
                    <div class="footer-head">
                        <h3>Quick Links :</h3>
                    </div>
                    <ul class="list-unstyled">
                        <li><a href="{{url('/')}}">Term and Condition</a></li>
                        <li><a href="{{url('/')}}">Privacy Policy</a></li>
                        <li><a href="{{url('/plan')}}">Plan</a></li>
                        <li><a href="{{url('/findpeople')}}">Find People</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="footer-block newsleter">
                    <div class="footer-head">
                        <h3>Subscribe Newsletter :</h3>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email Address">
                        <a href="javascript:void(0)" class="subscribe-btn">Subscribe</a>
                    </div>
                    <div class="social-nav">
                        <ul>
                            <li class="tw"><a href="javascript:void(0)"><i class="flaticon-twitter"></i></a></li>
                            <li class="fb"><a href="javascript:void(0)"><i class="flaticon-facebook-logo"></i></a></li>
                            <li class="gp"><a href="javascript:void(0)"><i class="flaticon-google-plus-logo"></i></a></li>
                            <li class="rss"><a href="javascript:void(0)"><i class="flaticon-rss-symbol"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright new-block">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">

                <p>2020 Design By <a href="javascript:void(0)"> Webmobril </a> All Rights Reserved</p>
            </div>
        </div>
    </div>

</div>
<span id="go_to_top" class="go-to-top"><i class="fa fa-long-arrow-up"></i></span>