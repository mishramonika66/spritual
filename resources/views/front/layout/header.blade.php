<header class="new-block header">
    <div class="topnav new-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="left-nav">
                        <a href="javascript:void(0)" class="support-num">Email : fanshub@gmail.com</a>
                    </div>
                    <div class="right-nav">
                        <span class="top-nav-signup_ligin"><a href="{{url('/signup')}}">SignUp</a> / <a href="{{url('/login')}}">Login</a></span>
                        <span class="top-nav-signup_ligin"><a href="{{url('/influencer')}}">Become a Influencer</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-nav new-block">
        <div class="container-fluid pad0">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="logo">
                        <a href="home.html"><img src="{{ url('/public/assets') }}/images/logo2.png" alt="logo" class="img-responsive"></a>
                    </div>
                    <a href="javascript:void(0)" class="nav-opener"><i class="fa fa-bars"></i></a>
                    <div class="nav-block">
                        <nav class="nav">
                            <ul class="list-unstyled">
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a href="{{ url('/feed') }}">Feeds</a></li>
                                <li><a href="{{ url('/findpeople') }}">Find People</a></li>
                                <li><a href="{{ url('/plan')}}">Plan</a></li>
                                <li><a href="{{ url('/faq')}}">Faq</a></li>
                                <li><a href="{{ url('/contact')}}">Contact</a></li>
                                <li><a href="{{ url('/about')}}">About</a></li>
                                <div class="side-sec"><span onclick="openNav()">Account</span></div>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>