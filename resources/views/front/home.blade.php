@extends('front.layout.app')

@section('title', 'Home | Fanshub')
<!--/ in case you want to write JS, write here/-->
@section('custom_js')
@parent
<!--/ in case you want to write JS, write here/-->


<!--/ in case you want to write JS, write here/-->
@endsection


@section('custom_css')
@parent
<!--/ in case you want to write CSS, write here/-->




<!--/ in case you want to write CSS, write here/-->
@endsection
@section('sidebar')
@parent
<!--/ in case you want add something to Sidebar, write here/-->



<!--/ in case you want add something to Sidebar, write here/-->
@endsection

@section('content')



<div id="slider" class="slider1 slider-stl owl-carousel owl-theme">
    <div class="item">
        <div class="slider-block new-block">
            <div class="fixed-bg" style="background: url('{{ url('/public/assets') }}/images/slide_img1.jpg');"></div>
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="slider-txt">
                            <div class="new-block cover-block">
                                <h1 class="" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutRight"><span>Welcome to</span> Fanshub</h1>
                            </div><!-- cover-block -->
                            <div class="new-block cover-block">
                                <p data-animation-in="fadeInDown" data-animation-out="animate-out fadeOutRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div><!-- cover-block -->
                            <div class="new-block cover-block">
                                <div class="btn-cover">
                                    <a href="javascript:void(0)" class="c-btn btn1" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">About Us</a>
                                </div>
                                <div class="btn-cover">
                                    <a href="javascript:void(0)" class="c-btn btn2" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">Contact Us</a>
                                </div>
                            </div><!-- cover-block -->
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5"><!-- logo-block -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="slider-block new-block">
            <div class="fixed-bg" style="background: url('{{ url('/public/assets') }}/images/slide_img4.jpg');"></div>
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="slider-txt">
                            <div class="new-block cover-block">
                                <h1 class="" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutRight"><span>Welcome to</span> Fanshub</h1>
                            </div><!-- cover-block -->
                            <div class="new-block cover-block">
                                <p data-animation-in="fadeInDown" data-animation-out="animate-out fadeOutRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div><!-- cover-block -->
                            <div class="new-block cover-block">
                                <div class="btn-cover">
                                    <a href="javascript:void(0)" class="c-btn btn1" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">About Us</a>
                                </div>
                                <div class="btn-cover">
                                    <a href="javascript:void(0)" class="c-btn btn2" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">Contact Us</a>
                                </div>
                            </div><!-- cover-block -->
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5"><!-- logo-block -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="slider-block new-block">
            <div class="fixed-bg" style="background: url('{{ url('/public/assets') }}/images/slide_img5.jpg');"></div>
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="slider-txt">
                            <div class="new-block cover-block">
                                <h1 class="" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutRight"><span>100% Free</span> Online Dating</h1>
                            </div><!-- cover-block -->
                            <div class="new-block cover-block">
                                <p data-animation-in="fadeInDown" data-animation-out="animate-out fadeOutRight">Connecting singles across the world to their ideal partner</p>
                            </div><!-- cover-block -->
                            <div class="new-block cover-block">
                                <div class="btn-cover">
                                    <a href="javascript:void(0)" class="c-btn btn1" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">View More</a>
                                </div>
                                <div class="btn-cover">
                                    <a href="javascript:void(0)" class="c-btn btn2" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">Register</a>
                                </div>
                            </div><!-- cover-block -->
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5"><!-- logo-block -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="why-join new-block pdtb-100">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title2">
                    <h2 class="fz35">How Fanshub Works</h2>
                    <div class="clearfix"></div>
                    <p class="fz20">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua. </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="block-syl1">
                    <i class="">1</i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="block-syl1">
                    <i class="">2</i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="block-syl1">
                    <i class="">3</i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                </div>
            </div>

        </div>
    </div>
</div>


<section class="online-member new-block ">


    <div class="clearfix"></div>
    <div class="filter-area new-block">
        <div class="container">
            <div class="row" id="MixItUp1">
                <div class="col-lg-12">
                    <div class="title2">
                        <h2 class="fz35">Our Premium</h2>

                    </div>
                </div>
                <div class="mix category-1 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img1.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="mix category-2 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img2.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="mix category-1 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img3.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="mix category-1 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img4.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="mix category-1 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img5.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="mix category-1 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img6.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="mix category-2 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img7.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="mix category-1 col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="block-stl2">
                        <div class="img-holder">
                            <img src="{{ url('/public/assets') }}/images/img8.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="txt-block">
                            <a href="javascript:void(0)">
                                <h3 class="fz22">Tenma Shyna</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- online-member -->

<section class="easy-connect new-block">
    <div class="fixed-bg parallax" style="background: url('{{ url('/public/assets') }}/images/pbg1.png');"></div>
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                <div class="connect-block text-center">
                    <h3>Want to join <span>the Fanshub?</span></h3>
                    <p class="fz35">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

                    <a href="javascript:void(0)" class="c-btn-ico btn2">Sign Up</a>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="member-feedback new-block pdtb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title2">
                    <h2 class="clr1 fz35">Testimonials</h2>
                    <div class="clearfix"></div>
                    <p class="fz20">Aliquam a neque tortor. Donec iaculis auctor turpis. Eporttitor<br> mattis ullamcorper urna. Cras quis elementum</p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="testimonial owl-carousel owl-theme">
                    <div class="item">
                        <div class="new-block">
                            <div class="block-stl3">
                                <i class="flaticon-left-quote"></i>
                                <p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
                                <i class="flaticon-right-quotation-sign"></i>
                                <div class="arrow-down"></div>
                            </div>
                            <div class="sm-block">
                                <div class="img-holder">
                                    <img src="{{ url('/public/assets') }}/images/tm1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="new-block">
                            <div class="block-stl3">
                                <i class="flaticon-left-quote"></i>
                                <p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
                                <i class="flaticon-right-quotation-sign"></i>
                                <div class="arrow-down"></div>
                            </div>
                            <div class="sm-block">
                                <div class="img-holder">
                                    <img src="{{ url('/public/assets') }}/images/tm2.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="new-block">
                            <div class="block-stl3">
                                <i class="flaticon-left-quote"></i>
                                <p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
                                <i class="flaticon-right-quotation-sign"></i>
                                <div class="arrow-down"></div>
                            </div>
                            <div class="sm-block">
                                <div class="img-holder">
                                    <img src="{{ url('/public/assets') }}/images/tm3.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="new-block">
                            <div class="block-stl3">
                                <i class="flaticon-left-quote"></i>
                                <p>Fusce vitae nisi sempe ultrices sapien nec, pharetra odio. Donec efficitur hendrerit</p>
                                <i class="flaticon-right-quotation-sign"></i>
                                <div class="arrow-down"></div>
                            </div>
                            <div class="sm-block">
                                <div class="img-holder">
                                    <img src="{{ url('/public/assets') }}/images/tm2.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="client-area new-block">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="block-stl4 bg1">
                    <img src="{{ url('/public/assets') }}/images/p1.png" style="width: 40%; margin: 0 auto;">
                    <p class="fz20">Premium Content</p>
                    <p style="font-size:14px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>
            <div class="col">
                <div class="block-stl4 bg2">
                    <img src="{{ url('/public/assets') }}/images/c.png" style="width: 40%;margin: 0 auto;">
                    <p class="fz20">Easy Payment Options</p>
                    <p style="font-size:14px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>
            <div class="col">
                <div class="block-stl4 bg3">
                    <img src="{{ url('/public/assets') }}/images/pay.png" style="width: 40%;margin: 0 auto;">
                    <p class="fz20">Join the Fanshub</p>
                    <p style="font-size:14px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>


        </div>
    </div>
</div>




@endsection