<?php

header("Accept:application/json");
header("content-type:application/json");

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/**
 * Routes Without Authentication
 */
Route::group(['as' => 'Version-1.NoAuth.', 'prefix' => 'v1'], function () {
    /*
     * Register API
     */
    Route::post('/register', 'Clients\ClientsController@create');
    /*
     * Login
     */
    Route::post('/login', 'OAuth2\LoginController@login');
    /*
     * 
     */
    Route::post('/socialLogin', 'Clients\ClientsController@socialLogin');
    /*
     * Refresh Token
     */
    Route::post('/login/refresh', 'OAuth2\LoginController@refresh');
    /*
     * Verify OTP Before Login Since OTP is getting used as the Password
     */
    Route::post('/authenticateMobile', 'OAuth2\MobileAuthController@verify');

    /*
     * Groups route ends 
     */


    /*
     * Cities routes start 
     */
    Route::get('/city', 'CityController@getCities');
    Route::post('/city', 'CityController@create');
    Route::put('/city/{id}', 'CityController@edit');
    Route::delete('/city/{id}', 'CityController@delete');

    /*
     * States routes start here 
     */
    Route::get('/state', 'StateController@getStates');
    Route::post('/state', 'StateController@create');
    Route::put('/state/{id}', 'StateController@edit');
    Route::delete('/state/{id}', 'StateController@delete');


    /*
     * Country routes starts here 
     */
    Route::get('/country', 'CountryController@getCountries');
    Route::post('/country', 'CountryController@create');
    Route::put('/country/{id}', 'CountryController@edit');
    Route::delete('/country/{id}', 'CountryController@delete');
});

/*
 * Routes with authentication!
 */

Route::group(['as' => 'Version-1.', 'prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    
    
    
    
    
});
