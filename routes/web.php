<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});


Route::get('/', 'FrontController@index');
Route::get('/feed', 'FeedController@index');
Route::get('/about', 'AboutController@index');
Route::get('/findpeople', 'FindPeopleController@index');
Route::get('/contact', 'ContactController@index');
Route::get('/faq', 'FaqController@index');
Route::get('/plan', 'PlanController@index');

Route::get('/influencer', 'InfluencerController@index');

Route::get('/login', 'Auth\LoginController@index');

Route::get('/signup', 'Auth\RegisterController@index');
Route::post('/create', 'Auth\RegisterController@create');




Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace

    Route::get('/dashboard', 'HomeController@index');
    Route::resource('users', 'UsersController');
    Route::resource('privacyPolicy', 'PrivacyPolicyController');
    Route::resource('faq', 'FaqController');
    Route::resource('terms', 'TermsController');
    Route::resource('aboutUs', 'AboutUsController');
    Route::resource('report', 'ReportController');
    Route::resource('email', 'EmailController');
    Route::resource('invoice', 'InvoiceController');
    Route::resource('setting', 'SettingController');
    Route::resource('notification', 'NotificationController');

//    route for category
    Route::get('/category', 'CategoryController@index');
    Route::get('/addcategory', 'CategoryController@create');
    Route::post('/store', 'CategoryController@store');

//    route for subcategory
    Route::get('/subcategory', 'SubCategoryController@index');
    Route::get('/addsubcategory', 'SubCategoryController@create');
    Route::post('/subcategorystore', 'SubCategoryController@store');

//    route for subcategory
    Route::get('/subsubcategory', 'SubSubCategoryController@index');
    Route::get('/addsubsubcategory', 'SubSubCategoryController@create');
    Route::post('/subsubcategorystore', 'SubSubCategoryController@store');
});
