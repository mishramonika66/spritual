<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\UserFactory;
use App\Libraries\CategoryFactory;
use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */
    protected $Categoryfactory;

    public function __construct(CategoryFactory $Categoryfactory, Request $request) {

        $this->Categoryfactory = $Categoryfactory;

        $this->request = $request;
    }

    public function index() {
        $data = [];
       $data['categories'] = $this->Categoryfactory->getCategory();
        
        return view('admin.category.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        $data = [];
      
            try {
                $banner = $this->Categoryfactory->addData($data);
                return redirect()->back()->with('message', 'Category has been added');
            } catch (Exception $e) {
                return $e->getMessage();
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = [];
        $data['user'] = $this->user->view($id);
        return view('admin.users.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $data = [];
        $data['user'] = $this->user->view($id);

        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        $data = $this->request->except(['_method', '_token']);
        $user = $this->user->update($id, $data);
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        //
    }

}
