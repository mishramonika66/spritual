<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\UserFactory;
use App\Libraries\SubSubCategoryFactory;
use App\Http\Controllers\Controller;
use App\Model\SubSubCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SubSubCategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */
    protected $SubSubCategoryFactory;

    public function __construct(SubSubCategoryFactory $SubSubCategoryFactory, Request $request) {

        $this->SubSubCategoryFactory = $SubSubCategoryFactory;

        $this->request = $request;
    }

    public function index() {
        $data = [];
       $data['subsubcategories'] = $this->SubSubCategoryFactory->getSubSubCategory();
        
        return view('admin.subsubcategory.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.subsubcategory.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        $data = [];
      
            try {
                $banner = $this->SubSubCategoryFactory->addData($data);
                return redirect()->back()->with('message', 'SubSubCategory has been added');
            } catch (Exception $e) {
                return $e->getMessage();
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = [];
        $data['user'] = $this->user->view($id);
        return view('admin.users.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $data = [];
        $data['user'] = $this->user->view($id);

        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        $data = $this->request->except(['_method', '_token']);
        $user = $this->user->update($id, $data);
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        //
    }

}
