<?php

namespace App\Model\PassportModel;

use Illuminate\Container\Container;
use App\Http\Libraries\PersonalAccessTokenFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PassportTokenGenerator
 *
 * @author Kamlesh Jha
 */
class PassportTokenGenerator {
    /*
     * @name : String
     * @client ID: INT, client id
     * $user: Object of userdetail will contain data from users table
     * Scope : permision array, for us not in use
     */

    public function createToken($name, $ClientObject = null, $user, array $scopes = []) {
        try {
            $TokenArray = Container::getInstance()
                    ->make(PersonalAccessTokenFactory::class)
                    ->make($user->getKey(), $name, $scopes, $ClientObject);
            return [
                'accessToken' => $TokenArray->accessToken,
                'revoked' => $TokenArray->token->revoked,
                'created_at' => $TokenArray->token->created_at,
                'expires_at' => $TokenArray->token->expires_at,
                'updated_at' => $TokenArray->token->updated_at,
                'claims' => $user
            ];
        } catch (\Exception $e) {
            return [
                $e->getCode(),
                $e->getFile(),
                $e->getMessage(),
                $e->getTrace()
            ];
        }
    }

}
