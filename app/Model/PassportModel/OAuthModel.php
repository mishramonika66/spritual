<?php

namespace App\Model\PassportModel;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Exception;
use DB;
use Crypt;

class OAuthModel extends Model {

    protected $table = 'oauth_clients';
    protected $fillable = [
        'user_id', 'name', 'secret', 'password_client', 'personal_access_client', 'redirect', 'revoked'
    ];

    public function user() {
        return $this->hasMany(\App\Model\Users\User::class);
    }

    /*
     * Array to Token
     */

    public function createNewOAuthClient($NewClientDataArray = Array()) {
        try {
            $this->user_id = $NewClientDataArray['id'];
            $this->name = isset($NewClientDataArray['name']) ? $NewClientDataArray['name'] : 'No Name';
            $this->secret = base64_encode(hash_hmac('sha256', $NewClientDataArray['uuid'], 'secret', true));
            $this->password_client = 1;
            $this->personal_access_client = 1;
            $this->redirect = isset($NewClientDataArray['redirect']) ? $NewClientDataArray['redirect'] : throwException('Redirect url is required');
            $this->revoked = 0;
            $this->save();
            $newlyInsertedClientId = $this->max('id');
            $this->createPesonalAccessClient($newlyInsertedClientId);
            return self::where('id', $newlyInsertedClientId)->first();
        } catch (\Exception $e) {
            return [
                $e->getCode(),
                $e->getFile(),
                $e->getMessage(),
                $e->getTraceAsString()
            ];
        }
    }

    public static function generateToken($Array) {
        try {
            return $Token = Crypt::encrypt(json_encode($Array));
        } catch (\Exception $e) {
            return [
                $e->getCode(),
                $e->getFile(),
                $e->getMessage(),
                $e->getTraceAsString()
            ];
        }
    }

    public static function generateOathClient($data) {
        try {
            $oauth_client = self::create([
                        'user_id' => $data['id'],
                        'name' => $data['name'],
                        'secret' => base64_encode(hash_hmac('sha256', $data['password'], 'secret', true)),
                        'password_client' => 1,
                        'personal_access_client' => 0,
                        'redirect' => '',
                        'revoked' => 0,
            ]);

            return $oauth_client;
        } catch (\Exception $e) {
            return [
                $e->getCode(),
                $e->getFile(),
                $e->getMessage(),
                $e->getTraceAsString()
            ];
        }
    }

    /*
     * Token To Array
     */

    public static function decodeToken($Token) {
        return $Array = json_decode(Crypt::decrypt($Token), true);
    }

    /*
     * To create paersonal access client when a new client will create
     * 
     */

    public function createPesonalAccessClient($ClientID) {

        try {
            DB::table('oauth_personal_access_clients')->insert([
                ['client_id' => $ClientID,
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]);
        } catch (\Exception $e) {
            return [
                $e->getCode(),
                $e->getFile(),
                $e->getMessage(),
                $e->getTraceAsString()
            ];
        }
    }

    public static function getClientIdAndSecretByUserId($userId) {
        try {
            return self::where('user_id', $userId)->first();
        } catch (\Exception $e) {
            return [
                $e->getCode(),
                $e->getFile(),
                $e->getMessage(),
                $e->getTraceAsString()
            ];
        }
    }

}
