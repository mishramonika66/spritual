<?php

/*
 *  @Author: KAMLESH JHA
 *  @Date: 
 *  @Org: Parangat Technologies
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of UserCity
 *
 * @author kamlesh
 */
class UserFirebaseToken extends Model {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','device_type', 'device_id', 'device_token', 'device_meta'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];

    public function user() {
        return $this->belongsTo(Users\User::class, 'user_id', 'id');
    }

}
