<?php

namespace App\Model\Client;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Exceptions\Exception;

/**
 * Description of Client
 *
 * @author raj
 */
class Client extends Model {

    //put your code here

    protected $fillable = [
        'uuid', 'name', 'email',
    ];

    public static function createClient($data) {
        try {
            $uuid4 = Uuid::uuid4();
            return self::create([
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'activated' => 0,
                        'uuid' => $uuid4->toString()
            ]);
        } catch (\Exception $e) {
            return Exception::Log($e);
        }
    }

    public static function getClients() {
        try {
            return self::all();
        } catch (\Exception $e) {
            return Exception::Log($e);
        }
    }

    public static function editClient($data) {

        try {
            self::where('uuid', $data['uuid'])
                    ->update([
                        'name' => $data['name'],
                        'updated_at' => Carbon::now()
            ]);
        } catch (\Exception $e) {
            return Exception::Log($e);
        }
    }

}
