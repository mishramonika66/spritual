<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use App\Model\SubSubCategory;
use Illuminate\Support\Facades\Auth;

class SubSubCategoryFactory {

    public $request;
    public $SubSubCategory;

    public function __construct(Category $SubSubCategory, Request $request) {
        $this->request = $request;
        $this->SubSubCategory = $SubSubCategory;
    }

    public function getAll() {
        $dataIndustries = City::orderBy('id', 'DESC')->get();
        return $dataIndustries;
    }

    public function getCountAll() {
        return City::count();
    }

    public function addData($data) {
        $this->request->validate([
            'name' => 'required',
            'image'=> 'required'
        ]);

        if (!empty($this->request->file('image'))) {


            $target = 'site_images/category_images/';
            $shortImage =$this->request->file('image');
            if (!empty($shortImage)) {
                $headerImageName = $shortImage->getClientOriginalName();
                $ext1 = $shortImage->getClientOriginalExtension();
                $temp1 = explode(".", $headerImageName);
                $newHeaderLogo = rand() . "" . round(microtime(true)) . "." . end($temp1);
                $short_imageTarget = 'site_images/category_images/' . $newHeaderLogo;
                $shortImage->move($target, $newHeaderLogo);
               
            }
        }

        $data = array(
            'name' => $this->request->post('name'),
            'status' => $this->request->post('status'),
            'image' =>  $short_imageTarget);
        
        SubSubCategory::create($data);
    }

    public function getById($id) {
        return SubSubCategory::where('id', $id)->get();
    }

    public function update($data) {
        $this->request->validate([
            'name' => 'required',
        ]);

        $id = $this->request->id;
        $title = $this->request->name;


        $user_id = Auth::user();


        $data = City::find($id);
        $data->name = $title;
        $data->status = 1;
        $data->updated_at = date('Y-m-d h:i:s');


        $data->save();
    }

    public function delete($id) {
        $getindustries = City::find($id);
        return $getindustries->delete();
    }

    public function SubSubCategory() {
        $SubSubCategory= SubSubCategory::all();

        return $SubSubCategory != NULL ? SubSubCategory : [];
    }

}
