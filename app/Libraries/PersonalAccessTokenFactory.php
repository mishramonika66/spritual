<?php

namespace App\Http\Libraries;

use Laravel\Passport\PersonalAccessTokenFactory as BaseTokenFactory; 
use App\Model\PassportModel\OAuthModel;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequest;
use Laravel\Passport\PersonalAccessTokenResult;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PersonalAccessCustomTokenFactory
 *
 * @author Kamlesh Jha
 */
class PersonalAccessTokenFactory extends BaseTokenFactory {
    //put your code here

    /**
     * Create a new personal access token.
     *
     * @param  mixed  $userId
     * @param  string  $name
     * @param  array  $scopes
     * @return \Laravel\Passport\PersonalAccessTokenResult
     */
    public function make($userId, $name, array $scopes = []) {

        $response = $this->dispatchRequestToAuthorizationServer(
                $this->createRequest($this->clients->personalAccessClient(), $userId, $scopes)
        );

        $token = tap($this->findAccessToken($response), function ($token) use ($userId, $name) {
            $this->tokens->save($token->forceFill([
                        'user_id' => $userId,
                        'name' => $name,
            ]));
        });

        return new PersonalAccessTokenResult(
                $response['access_token'], $token
        );
    }

    /**
     * Create a request instance for the given client.
     *
     * @param  \Laravel\Passport\Client  $client
     * @param  mixed  $userId
     * @param  array  $scopes
     * @return \Zend\Diactoros\ServerRequest
     */
    protected function createRequest($ClientObject = false, $userId, array $scopes) {
        $Client = $ClientObject == NULL ? OAuthModel::getClientIdAndSecretByUserId($userId) : $ClientObject;
        /**
         * Will always be an instance of Zend\Diactoros\ServerRequest
         */
        return (new ServerRequest)->withParsedBody([
                    'grant_type' => 'personal_access',
                    'client_id' => $Client->id,
                    'client_secret' => $Client->secret,
                    'user_id' => $userId,
                    'scope' => implode(' ', $scopes),
        ]);
    }

    protected function dispatchRequestToAuthorizationServer(ServerRequest $request) {
        return json_decode($this->server->respondToAccessTokenRequest(
                        $request, new Response
                )->getBody()->__toString(), true);
    }

}
