<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Libraries;
use App\User;
use Ramsey\Uuid\Uuid;

/**
 * Description of ShopFactory
 *
 * @author Anu
 */
class UserFactory {

    
    public function __construct() {
       
    }

    public function create(array $data) {

        
    }
    
    public function getUser() {
        $user =  User::where('role_id','=',2)->paginate(20);
        return isset($user)?$user:[];
    }
        
    public function view($id){
        $user =  User::where('uuid',$id)->first();
        
        return isset($user)?$user:[];
    }
    public function update($id='', array $data) {
        $User = User::where('uuid',$id);
        $data['uuid'] = $id;
        if (isset($User) && ($data != null)) {
            return $User->update($data);
        }else{
            return false;
        }

        
    }
    
    public function deleteUser() {

        
    }
    
    
}
