<?php

/*
 *  @Author: Sakshi
 *  @FileName: 
 *  @Date: 
 */

/**
 * Description of Pushfactory
 *
 * @author parangat-pt-09
 */

namespace App\Libraries;

use Mail;
use Illuminate\Support\Facades\Log;

class Pushfactory {

    //put your code here

    public static function sendNotification($DeviceToken = '', $AppPlatform = '', $PushArr = []) {
        $Data = [];
        $DataArray = [
            "sound" => 'default',
            "content-available" => 1,
        ];
        $FinalArr = array_merge($DataArray, $PushArr);
        try {
            switch ($AppPlatform) {
                case 1: // ANDROID
                    $Data = self::pushNotificationsForAndroid($DeviceToken, $FinalArr);
                    break;
                case 2: // IOS 
                    $Data = self::pushNotificationsForIos($DeviceToken, $FinalArr);
                    break;
                default:
                    $Data = 'Failed to get device type';
                    break;
            }
            return $Data;
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public static function pushNotificationsForAndroid($DeviceToken, $DataArray) {

        try {
//            $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
//            $fields = [
//                'to' => $DeviceToken,
//                'data' => $DataArray
//            ];
//            $headers = [
//                'Authorization:key=AAAAatMCksE:APA91bFpD0uIBzbSXTyTk7_hVmgRYAQCYnwXCh_xa_RC1EiAfW-LamhfvfG28wDiUSmZkmpI7CD-qGwILprqjglwTy9ZAw_19iX7z7OTucwUT5rQl0f4VnBFQquH5b-I_kBqnE-zfnog',
//                'Content-Type:application/json'
//            ];
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
//            curl_setopt($ch, CURLOPT_POST, true);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//            $result = curl_exec($ch);
//            curl_close($ch); 
//            Log::error("PUSH FACTORY :: ",['fields' => $fields, 'result' => $result]);
//            return ['fields' => $fields, 'result' => $result];
//            return $fields;
            return json_decode($result);
        } catch (Exception $ex) {
            return [
                'code' => $ex->getCode(),
                'message' => $ex->getMessage()
            ];
        }
    }

    public static function pushNotificationsForIos($DeviceToken, $DataArray) {
        try {

//            $payload = array();
//            $payload['aps'] = $DataArray;
//            $payload = json_encode($payload);
//            $apnsCert = getcwd() . 'abc.pem';
//            $apnsHost = 'gateway.sandbox.push.apple.com';
//            $apnsPort = 2195;
//            $streamContext = stream_context_create();
//            stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
//            $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
//            $device_token = str_replace(' ', '', trim($DeviceToken));
//            $apnsMessage = chr(0) . @pack('n', 32) . @pack('H*', $device_token) . @pack('n', strlen($payload)) . $payload;
//            $result = fwrite($apns, $apnsMessage);
//            fclose($apns);
            return json_encode([$apns, $apnsMessage]);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public static function send_email($PushArr) {
        Mail::send('emails.confirmation', $PushArr, function($message) {
            $message->to('kamlesh.j@parangat.com', 'MEG V2')->subject('Test mail');
            $message->from('no-reply@meg-123.com', 'MEG V2');
        });
    }

}
